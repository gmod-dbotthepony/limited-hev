
-- Copyright (C) 2016-2023 DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local function SettingsMenuClient(pnl)
    local ConVars = {
        'cl_limited_oxygen_hud',
        'cl_limited_flashlight_hud',
        'cl_longjump_hud',
    }

    local ConVarsDefault = {}
    for _, ConVar in ipairs(ConVars) do
        ConVarsDefault[ConVar] = GetConVar(ConVar):GetDefault()
    end

    pnl:AddControl('ComboBox', {MenuButton = 1, Folder = 'limitedhev_client', Options = {['#preset.default'] = ConVarsDefault}, CVars = ConVars})

    pnl:CheckBox('gui.hev.menu.client.oxygen_hud', 'cl_limited_oxygen_hud')
    pnl:CheckBox('gui.hev.menu.client.flashlight_hud', 'cl_limited_flashlight_hud')
    pnl:CheckBox('gui.hev.menu.client.longjump_hud', 'cl_longjump_hud')
end

local function SettingsMenuServer(pnl)
    local ConVars = {
        'sv_limited_oxygen_restore',
        'sv_limited_oxygen_restorer',
        'sv_limited_oxygen_restores',
        'sv_limited_oxygen_restorep',
        'sv_limited_oxygen_choke',
        'sv_limited_oxygen_choke_dmg',
        'sv_limited_oxygen_r',
        'sv_limited_oxygen_r_mul',
        'sv_limited_hev_rd',
        'sv_limited_hev_rmul',

        'sv_limited_sprint',
        'sv_limited_sprint_mmod',
        'sv_limited_hev_slim',
        'sv_limited_hev_sprint',
        
        'sv_limited_flashlight',
        'sv_limited_flashlight_ratio',
        'sv_limited_flashlight_restore_ratio',
        'sv_limited_flashlight_pause',
        'sv_limited_flashlight_epause',
        
        'sv_longjump_enabled',
        'sv_longjump_power',
        'sv_longjump_amount',
        'sv_longjump_cooldown',
        'sv_longjump_delay',
    }

    local ConVarsDefault = {}
    for _, ConVar in ipairs(ConVars) do
        ConVarsDefault[ConVar] = GetConVar(ConVar):GetDefault()
    end

    pnl:AddControl('ComboBox', {MenuButton = 1, Folder = 'limitedhev_server', Options = {['#preset.default'] = ConVarsDefault}, CVars = ConVars})

    local oxygenpnl = vgui.Create('DForm', pnl)
    oxygenpnl:SetName('gui.hev.menu.server.oxygen_label')
    pnl:AddItem(oxygenpnl, nil)

    oxygenpnl:CheckBox('gui.hev.menu.server.oxygen', 'sv_limited_oxygen')
    
    oxygenpnl:CheckBox('gui.hev.menu.server.oxygen_restorehp', 'sv_limited_oxygen_restore')
    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_restorehp_maxmul', 'sv_limited_oxygen_restorer', 0, 4, 2)
    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_restorehp_amt', 'sv_limited_oxygen_restores', 0, 100, 0)
    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_restorehp_delay', 'sv_limited_oxygen_restorep', 0, 60, 2)

    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_drown_rate', 'sv_limited_oxygen_choke', 0, 60, 2)
    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_drown_dmg', 'sv_limited_oxygen_choke_dmg', 0, 100, 0)

    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_drain_ratio', 'sv_limited_oxygen_r', 0, 4, 3)
    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_drain_ratio_nosuit', 'sv_limited_oxygen_r_mul', 0, 12, 3)

    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_restore_delay', 'sv_limited_hev_rd', 0, 60, 2)
    oxygenpnl:NumSlider('gui.hev.menu.server.oxygen_restore_ratio', 'sv_limited_hev_rmul', 0, 4, 3)

    local sprintpnl = vgui.Create('DForm', pnl)
    sprintpnl:SetName('gui.hev.menu.server.sprint_label')
    pnl:AddItem(sprintpnl, nil)

    sprintpnl:CheckBox('gui.hev.menu.server.sprint', 'sv_limited_sprint')
    sprintpnl:CheckBox('gui.hev.menu.server.sprint_mmod', 'sv_limited_sprint_mmod')
    sprintpnl:Help('gui.hev.menu.server.sprint_mmod_help')
    
    sprintpnl:NumSlider('gui.hev.menu.server.sprint_power_min', 'sv_limited_hev_slim', 0, 100, 2)
    sprintpnl:NumSlider('gui.hev.menu.server.sprint_power_drain_mul', 'sv_limited_hev_sprint', 0, 4, 3)

    local flashlightpnl = vgui.Create('DForm', pnl)
    flashlightpnl:SetName('gui.hev.menu.server.flashlight_label')
    pnl:AddItem(flashlightpnl, nil)

    flashlightpnl:CheckBox('gui.hev.menu.server.flashlight', 'sv_limited_flashlight')
    flashlightpnl:NumSlider('gui.hev.menu.server.flashlight_power_draw_pct', 'sv_limited_flashlight_ratio', 0, 400, 1)
    flashlightpnl:NumSlider('gui.hev.menu.server.flashlight_power_restore_pct', 'sv_limited_flashlight_restore_ratio', 0, 2000, 1)
    flashlightpnl:NumSlider('gui.hev.menu.server.flashlight_power_restore_delay', 'sv_limited_flashlight_pause', 0, 60, 2)
    flashlightpnl:NumSlider('gui.hev.menu.server.flashlight_recover_delay', 'sv_limited_flashlight_epause', 0, 60, 2)

    local longjumppnl = vgui.Create('DForm', pnl)
    longjumppnl:SetName('gui.hev.menu.server.longjump_label')
    pnl:AddItem(longjumppnl, nil)

    longjumppnl:CheckBox('gui.hev.menu.server.longjump', 'sv_longjump_enabled')
    longjumppnl:NumSlider('gui.hev.menu.server.longjump_power', 'sv_longjump_power', 0, 4, 3)
    longjumppnl:NumSlider('gui.hev.menu.server.longjump_amount', 'sv_longjump_amount', 1, 10, 0)
    longjumppnl:NumSlider('gui.hev.menu.server.longjump_cooldown', 'sv_longjump_cooldown', 0, 60, 2)
    longjumppnl:NumSlider('gui.hev.menu.server.longjump_delay', 'sv_longjump_delay', 0, 60, 2)
end

hook.Add('PopulateToolMenu', 'LimitedHEVSettings', function()
    spawnmenu.AddToolMenuOption('Utilities', 'User', 'LimitedHEV.Client', 'gui.hev.menu.client.name', '', '', SettingsMenuClient)
    spawnmenu.AddToolMenuOption('Utilities', 'Admin', 'LimitedHEV.Server', 'gui.hev.menu.server.name', '', '', SettingsMenuServer)
end)