
-- The Unlicense (no Copyright) DBotThePony
-- do whatever you want, including removal of this notice

gui.hev.power = 'HEV Power'
gui.hev.flashlight = 'Flashlight'

gui.hev.menu.client.name = 'Limited HEV'

gui.hev.menu.client.oxygen_hud = 'Enable Power HUD'
gui.hev.menu.client.flashlight_hud = 'Enable Flashlight HUD'
gui.hev.menu.client.longjump_hud = 'Enable Long Jump HUD'

gui.hev.menu.server.name = 'Limited HEV'

gui.hev.menu.server.oxygen_label = 'Limited Oxygen'
gui.hev.menu.server.oxygen = 'Enable Limited Oxygen'
gui.hev.menu.server.oxygen_restorehp = 'Drowning Damage Recover'
gui.hev.menu.server.oxygen_restorehp_maxmul = 'Health Recover Multiplier'
gui.hev.menu.server.oxygen_restorehp_amt = 'Health Recover Amount (per second)'
gui.hev.menu.server.oxygen_restorehp_delay = 'Health Recover Delay'
gui.hev.menu.server.oxygen_drown_rate = 'Drowning Tick Delay'
gui.hev.menu.server.oxygen_drown_dmg = 'Drowning Damage Amount'
gui.hev.menu.server.oxygen_drain_ratio = 'Power Drain Multiplier'
gui.hev.menu.server.oxygen_drain_ratio_nosuit = 'No Suit Power Drain Multiplier'
gui.hev.menu.server.oxygen_restore_delay = 'Power Recover Delay'
gui.hev.menu.server.oxygen_restore_ratio = 'Power Recover Multiplier'

gui.hev.menu.server.sprint_label = 'Limited Sprint'
gui.hev.menu.server.sprint = 'Enable Limited Sprint'
gui.hev.menu.server.sprint_mmod = 'MMod-Style Limited Sprint'
gui.hev.menu.server.sprint_mmod_help = 'Requires full power recharge after draining to start sprinting again.'
gui.hev.menu.server.sprint_power_min = 'Minimum Power for Sprint'
gui.hev.menu.server.sprint_power_drain_mul = 'Power Drain Multiplier'

gui.hev.menu.server.flashlight_label = 'Limited Flashlight'
gui.hev.menu.server.flashlight = 'Enable Limited Flashlight'
gui.hev.menu.server.flashlight_power_draw_pct = 'Power Drain Percentage'
gui.hev.menu.server.flashlight_power_restore_pct = 'Power Restore Percentage'
gui.hev.menu.server.flashlight_power_restore_delay = 'Power Restore Delay'
gui.hev.menu.server.flashlight_recover_delay = 'Delay After Full Drain'

gui.hev.menu.server.longjump_label = 'Long Jump'
gui.hev.menu.server.longjump = 'Enable Long Jump'
gui.hev.menu.server.longjump_power = 'Power Multiplier'
gui.hev.menu.server.longjump_amount = 'Amount of Jumps'
gui.hev.menu.server.longjump_cooldown = 'Long Jump Recharge Time'
gui.hev.menu.server.longjump_delay = 'Long Jump Delay'
