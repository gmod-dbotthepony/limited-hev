
-- The Unlicense (no Copyright) DBotThePony
-- do whatever you want, including removal of this notice

gui.hev.power = 'Заряд HEV'
gui.hev.flashlight = 'Фонарик'

gui.hev.menu.client.oxygen_hud = 'Включить HUD заряда костюма'
gui.hev.menu.client.flashlight_hud = 'Включить HUD заряда фонарика'
gui.hev.menu.client.longjump_hud = 'Включить HUD длинных прыжков'

gui.hev.menu.server.oxygen_label = 'Ограниченный воздух'
gui.hev.menu.server.oxygen = 'Включить ограниченный воздух'
gui.hev.menu.server.oxygen_restorehp = 'Восстановление здоровья после утопления'
gui.hev.menu.server.oxygen_restorehp_maxmul = 'Максимальный множитель здоровья для восстановления'
gui.hev.menu.server.oxygen_restorehp_amt = 'Восстановление здоровья (в секунду)'
gui.hev.menu.server.oxygen_restorehp_delay = 'Задержка перед восстановлением здоровья'
gui.hev.menu.server.oxygen_drown_rate = 'Время между уроном от утопления'
gui.hev.menu.server.oxygen_drown_dmg = 'Урон от утопления'
gui.hev.menu.server.oxygen_drain_ratio = 'Множитель потребления заряда'
gui.hev.menu.server.oxygen_drain_ratio_nosuit = 'Потребление заряда без костюма'
gui.hev.menu.server.oxygen_restore_delay = 'Задержка восстановления заряда'
gui.hev.menu.server.oxygen_restore_ratio = 'Множитель восстановления заряда'

gui.hev.menu.server.sprint_label = 'Ограниченный бег'
gui.hev.menu.server.sprint = 'Включить ограниченный бег'
gui.hev.menu.server.sprint_mmod = 'Лимит в стиле MMod'
gui.hev.menu.server.sprint_mmod_help = 'Для бега требуется полный заряд костюма после опустошения.'
gui.hev.menu.server.sprint_power_min = 'Минимальный заряд для бега'
gui.hev.menu.server.sprint_power_drain_mul = 'Множитель потребления заряда'

gui.hev.menu.server.flashlight_label = 'Ограниченный фонарь'
gui.hev.menu.server.flashlight = 'Включить ограниченный фонарь'
gui.hev.menu.server.flashlight_power_draw_pct = 'Множитель потребления заряда'
gui.hev.menu.server.flashlight_power_restore_pct = 'Множитель восстановления заряда'
gui.hev.menu.server.flashlight_power_restore_delay = 'Задержка перед восстановлением заряда'
gui.hev.menu.server.flashlight_recover_delay = 'Задержка включения после опустошения заряда'

gui.hev.menu.server.longjump_label = 'Длинные прыжки'
gui.hev.menu.server.longjump = 'Включить длинные прыжки'
gui.hev.menu.server.longjump_power = 'Сила длинных прыжкой'
gui.hev.menu.server.longjump_amount = 'Количество прыжков'
gui.hev.menu.server.longjump_cooldown = 'Время перезарядки прыжка'
gui.hev.menu.server.longjump_delay = 'Задержка между прыжками'
